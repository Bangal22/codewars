package level4;

public class Lcs {
    public static String lcs(String a, String b) {
        int aLength = a.length()+1;
        int bLength = b.length()+1;

        char [] arrayA = a.toCharArray();
        char [] arrayB = b.toCharArray();

        int [][] board = new int[aLength][bLength];

        for (int i = 0; i < arrayB.length; i++) {
            for (int j = 0; j < arrayA.length; j++) {
                if (arrayB[i] == arrayA[j]) {
                    board[j+1][i+1] = board[j][i]+1;
                }
                else board[j + 1][i + 1] = Math.max(board[j][i + 1], board[j + 1][i]);
            }
        }

        StringBuilder result = new StringBuilder(backtracking(board, arrayA, arrayB, aLength-2, bLength-2, "")).reverse();
        return result.toString();
    }

    public static String backtracking (int [][]board, char [] arrayA ,char [] arrayB, int a, int b, String result) {

        if(a < 0 || b < 0) return result;

        if(arrayB[b] == arrayA[a]) {
            result += arrayB[b];
            a--;
            b--;
            return backtracking(board, arrayA, arrayB, a, b, result);
        }
        if (board[a][1+b] >= board[1+a][b]){
            a--;
            return backtracking(board, arrayA, arrayB, a, b, result);
        }
        if (board[a][1+b] <= board[1+a][b]){
            b--;
            return backtracking(board, arrayA, arrayB, a, b, result);
        }

        return "";
    }

    public static void subsequence(int[][] C, int a, int b){
        for (int i = 0; i < b; i++) {
            System.out.println("\n");
            for (int j = 0; j < a; j++) {
                System.out.print(C[j][i] + "\t");
            }
        }
    }
    public static void main (String[] args) {
        //String result = lcs("abcdef", "abc");
        String result = lcs("abcdefghijklmnopq", "apcdefghijklmnobq");
        System.out.println(result);
    }
}
