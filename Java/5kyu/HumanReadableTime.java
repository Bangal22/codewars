package level5;

public class HumanReadableTime {
    public static String makeReadable(int seconds) {
        int hor = 0;
        int min = 0;
        int sec = 0;
        for (int i = 0; i < seconds; i++) {
            sec++;
            if(sec > 59){ sec = 0; min++; }
            if(min > 59){ min = 0; hor++; }
        }
        String h = hor<10 ? "0" + hor : String.valueOf(hor);
        String m = min<10 ? "0" + min : String.valueOf(min);
        String s = sec<10 ? "0" + sec : String.valueOf(sec);
        return h + ":" + m +":" + s;
    }

    public static void main (String[] args) {
        String result = makeReadable(359999);
        System.out.println(result);
    }
}
