package level5;

import java.util.Arrays;

public class PascalDiagonals {
    public static void main (String[] args) {
        generateDiagonal(36,11);
    }
    public static long[] generateDiagonal(int n, int l) {
        long [] result = new long[l];
        long [][] pascalBoard = new long[l+n][l+n];
        int index = n;

        for (int i = 0; i < pascalBoard.length; i++) {
            pascalBoard[i][0] = 1;
            if (i == 0) { continue; }
            for (int j = 0; j < pascalBoard.length; j++) {
                if (j == pascalBoard.length-1) {continue;}
                long sum = pascalBoard[i-1][j] + pascalBoard[i-1][j+1];
                pascalBoard[i][j+1] =  sum;
            }
        }

        for (int i = 0; i < pascalBoard.length-n; i++){
            result[i] = pascalBoard[index][i];
            index++;
        }
        return result;
    }
}
