package level5;

public class SecureTester {

    public static boolean alphanumeric(String s){
        return s.matches("[\\w]+");

    }

    public static void main (String[] args) {
        boolean result = alphanumeric("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789");
        System.out.println(result);
    }
}
