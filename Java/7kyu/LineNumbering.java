package level7;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LineNumbering {
    public static List<String> number(List<String> lines) {
        List<String> okList = new ArrayList<>();
        for (int i = 0; i < lines.size(); i++) {
            String flow = i+1 + ": " + lines.get(i);
            okList.add(flow);
        }
        return okList;
    }

    public static void main (String[] args) {
        number(Arrays.asList("", "", "", "", ""));
    }
}
