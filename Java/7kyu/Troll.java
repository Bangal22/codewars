package level7;

public class Troll {
    public static String disemvowel(String str) {
        String [] spltStr = str.split("");
        String concat = "";
        for (String x: spltStr) {
            String lletra = x.toUpperCase();
            if(!(lletra.equals("A") || lletra.equals("E") || lletra.equals("I") || lletra.equals("O") || lletra.equals("U"))){
                concat += x;
            }
        }
        return concat;
    }

    public static void main (String[] args) {
        disemvowel("LOL");
    }

}
