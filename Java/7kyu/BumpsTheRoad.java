package level7;

import java.util.Arrays;

public class BumpsTheRoad {
    public static String bumps(final String road) {
        char [] Bumps = road.toCharArray();
        int counter = 0;
        for (char x : Bumps)
            if( x == 'n' ) counter++;
        return counter<15 ? "Woohoo!" : "Car Dead";
    }

    public static void main (String[] args) {
        System.out.println(bumps("_nnnnnnn_n__n______nn__nn_nnn"));
    }
}


