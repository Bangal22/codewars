package level7;

import java.util.ArrayList;

class Metro {

    public static int countPassengers(ArrayList<int[]> stops) {

        int total = 0;
        for (int[] stop : stops) { total -= (stop[1] - stop[0]);}
        return total;
    }


    public static void main (String[] args) {
        ArrayList<int[]> list = new ArrayList<int[]>();
        list.add(new int[] {10,0});
        list.add(new int[] {3,5});
        list.add(new int[] {2,5});

        System.out.println(countPassengers(list));
    }
}
