package level6;

import java.util.Arrays;

public class WhichAreIn {

    public static String[] inArray(String[] array1, String[] array2) {
        String names = "";
        for (String x : array1) {
            for (String y: array2) {
                char [] a = x.toCharArray();
                char [] b = y.toCharArray();
                int counter = 0;
                int indexA = 0;
                int indexB = 0;
                boolean activate = false;

                while (x.length()>counter && indexA < x.length() && indexB < y.length()){
                    if(a[indexA] == b[indexB]){
                        counter++;indexA++;indexB++;activate = true;
                    }else{
                        indexB++;counter = 0;activate = false;
                    }
                }
                if(activate && counter==x.length()){
                    String [] repetits = names.split(" ");
                    boolean find = false;
                    for (String r: repetits) {
                        if (r.equals(x)) {
                            find = true;
                            break;
                        }
                    }
                    if(!find) names += x + " ";
                }
            }
        }

        String [] separteNames = names.split(" ");

        int index = 0;
        String post;
        while(index <= separteNames.length){
            for (int i = 0; i < separteNames.length; i++) {
                if(i+1 >= separteNames.length) break;
                if(separteNames[i].compareTo(separteNames[i+1]) > 0){
                    post   = separteNames[i];
                    separteNames[i]  = separteNames[i+1];
                    separteNames[i+1]= post;
                }
            }
            index++;
        }
        if( separteNames.length == 1 && separteNames[0].equals("")) separteNames = new String[]{};
        return separteNames;
    }


    public static void main (String[] args) {
        String[] a1 = {"tarp", "mice", "bull"};
        String[] a2 = {"lively", "alive", "harp", "sharp", "armstrong"};
        //String[] a1 = {"pillow","cod", "code", "wars", "ewar", "bed", "phht"};
        //String[] a2 = {"lively", "alive", "harp", "sharp", "armstrong", "codewars", "cod", "code"};
        System.out.println(Arrays.toString(inArray(a1, a2)));
    }


}
