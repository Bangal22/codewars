package level6;

import java.util.ArrayList;
import java.util.Arrays;

public class MexicanWave {
    public static String[] wave(String str) {
        ArrayList<String> letersUpper = new ArrayList<>();
        String [] strSplit = str.split("");
        for (int i = 0; i < strSplit.length; i++) {
            String cadaLletra = "";
            if(i-1 >= 0){
                strSplit[i] = strSplit[i].toUpperCase();
                strSplit[i-1] = strSplit[i-1].toLowerCase();
            }else strSplit[i] = strSplit[i].toUpperCase();

            if(!strSplit[i].equals(" ")){
                for (String x : strSplit)  cadaLletra += x;
                letersUpper.add(cadaLletra);
            }

        }

        String[] b = new String[letersUpper.size()];
        b = letersUpper.toArray(b);
        System.out.println(b.length);
        return b;
    }

    public static void main (String[] args) {
        System.out.println(Arrays.toString(wave(" ")));
    }
}
