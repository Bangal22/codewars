package level6;

import java.awt.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class FindOdd {
    public static int findIt(int[] a) {
            int mainOdd = 0;
            HashMap <Integer, Integer> odd = new HashMap<>();
            for (int x: a) {
                if(!odd.containsKey(x)) odd.put(x, 0);
                odd.replace(x, odd.get(x)+1);
            }

            for (Integer y : odd.values()){
                if(y%2!=0 && mainOdd < y) mainOdd = y;
            }
            return mainOdd;
    }

    public static void main (String[] args) {
        int result = findIt(new int[]{0,1,0,1,0} );
        System.out.println(result);
    }
}
