package level6;

import java.util.HashMap;

public class Kata {
    public static int numberOfPairs(String[] gloves) {
        HashMap <String, Integer > saveColors = new HashMap<>();
        int count = 0;
        for (String colors : gloves) {
            if(!saveColors.containsKey(colors)) saveColors.put(colors,1);
            else{
                saveColors.replace(colors,saveColors.get(colors), saveColors.get(colors) +1);
                if(saveColors.get(colors) > 1){
                    saveColors.replace(colors,saveColors.get(colors),0);
                    count++;
                }
            }
        }
        return count;
    }

    public static void main (String[] args) {
        String [] inputs = {"red", "green", "red", "blue", "blue"};
        int result = numberOfPairs(inputs);
        System.out.println(result);
    }
}
