package level6;

import java.util.Arrays;

public class BackspacesInString {
    public String cleanString(String s) {
        char [] characters = s.toCharArray();
        String result = "";

        for (int i = 0; i < characters.length; i++) {
            if (characters[i] == '#'){
                for (int j = i; j >= 0; j--) {
                    if (characters[j] != ' ' && characters[j] != '#') {
                        characters[j] = ' ';
                        break;
                    }
                }
            }
        }

        for (char x : characters) if (x != ' ' && x != '#') result+=x;

        return result;
    }
    public static void main (String[] args) {
        BackspacesInString s = new BackspacesInString();
        s.cleanString("abc##d######");
    }
}
