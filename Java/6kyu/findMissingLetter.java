package level6;

public class findMissingLetter {
    public static char findMissingLetter (char[] array){
        char [] up = new char []{'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
        char [] down = new char []{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};

        return array[0] > 64 && array[0] < 91 ? findLetter(array, up) : findLetter(array, down);

    }

    public static char findLetter (char [] array, char [] arrayToFind){
        int index = 0;
        char missing = 0;
        for (char c : arrayToFind) {
            if (array.length == index) break;
            if (c == array[index]) index++;
            else missing = c;
        }

        return missing;
    }

    public static void main (String[] args) {
        char data = findMissingLetter(new char[]{'a', 'b', 'c', 'd', 'f'});
        System.out.println(data);
    }
}
