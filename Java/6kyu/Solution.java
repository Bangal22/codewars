package level6;

import java.util.Arrays;

public class Solution {
    static String toCamelCase(String s){
        String [] d = s.split("-");
        String [] e = s.split("_");
        String result = "";
        if(d.length > e.length) result = transformar(d);
        else result = transformar(e);
        return result;
    }

    static String transformar (String [] s){
        StringBuilder d = new StringBuilder();
        for (int i = 0; i < s.length; i++) {
            if(i !=0 ){
                char[] c = s[i].toCharArray();
                String f = String.valueOf(c[0]).toUpperCase();
                c[0] = f.charAt(0);
                for (char l : c) d.append(l);
            }else d.append(s[i]);
        }
        return d.toString();
    }

    public static void main (String[] args) {
        String s  = toCamelCase("the-stealth-warrior");

    }
}
