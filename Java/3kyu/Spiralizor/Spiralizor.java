package level3;

public class Spiralizor {
    public static void main (String[] args) {
       spiralize(7);

    }
    public static int[][] spiralize(int size) {
        int x = 0;
        int y = 0;
        int [][] board = new int[size][size];
        board = backtracking(board, x,y,size-1, 0,false, true, false, false);
        return board;
    }

    public static int[][] backtracking(
            int[][]board,
            int x,
            int y,
            int size,
            int limit,
            boolean w,
            boolean d,
            boolean s,
            boolean a
    ) {

        board[x][y] = 1;

        if ( y >= size && d ){
            d = false;
            s = true;
        }
        else if ( x >= size && s ){
            s = false;
            a = true;
        }
        else if ( limit == y && a ) {
            a = false;
            w = true;
            limit+=2;
        }
        else if ( x == limit && w ) {
            w = false;
            d = true;
            size -= 2;
        }

        if (w) {
            if( board[x-1][y+1] == 1){
                if( board[x-1][y] == 1) board[x][y] = 0;
                return board;
            }
            return backtracking(board,x-1,y,size,limit,w,d,s,a);
        }
        if (d) {
            if( board[x+1][y+1] == 1){
                if( board[x][y+1] == 1) board[x][y] = 0;
                return board;
            }
            return backtracking(board,x,y+1,size,limit,w,d,s,a);
        }
        if (s) {
            if( board[x+1][y-1] == 1) {
                if( board[x][y-1] == 1) board[x][y] = 0;
                return board;
            }
            return backtracking(board,x+1,y,size,limit,w,d,s,a);
        }
        if (a) {
            if( board[x-1][y-1] == 1) {
                if( board[x+1][y] == 1) board[x][y] = 0;
                return board;
            }
            return backtracking(board,x,y-1,size,limit,w,d,s,a);
        }

        return board;
    }
}
