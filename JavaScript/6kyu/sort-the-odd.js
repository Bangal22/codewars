function sortArray(array) {
    let oddNumbers = array.filter(number => number %2 != 0).sort( (a,b) => a-b )
    let oddIndex = 0;
    for (const key in array) {
        if(array[key] %2 != 0) {
            array[key] = oddNumbers[oddIndex]
            oddIndex++
        }
    }
    return array;
}