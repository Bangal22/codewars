function digital_root(n) {
    let num = n.toString().split('');
    while (num.length > 1) num = num.reduce((pr, pos) => parseInt(pr) + parseInt(pos)).toString().split('');
    return parseInt(num[0]);
}