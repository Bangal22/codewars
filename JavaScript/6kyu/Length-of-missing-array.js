function getLengthOfMissingArray(arrayOfArrays) {

    if (!arrayOfArrays || arrayOfArrays.includes(null) || !arrayOfArrays.length) return 0

    let indexes = arrayOfArrays.map(array => array.length).sort((a, b) => a - b);

    let counter = indexes[0];
    for (const number of indexes) {
        if (number != counter || !number) return counter
        counter++
    }

    return 0
}