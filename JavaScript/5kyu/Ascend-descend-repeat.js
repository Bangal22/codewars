function ascendDescend(length, minimum, maximum) {
    let result = "";
    let reverse = false;
    let index = minimum;

    if (maximum < minimum || !length) return ''
    if (minimum == maximum) return Array(length).fill(minimum).join('');

    while (result.split('').length < length) {
        result += index
        if (index == minimum) reverse = false
        if (index == maximum) reverse = true

        reverse ? index-- : index++
    }

    return result.slice(0, length)

}