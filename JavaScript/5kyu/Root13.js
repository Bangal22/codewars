const ABC = Array.from('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz');
const ABC_ENCODE = Array.from('NOPQRSTUVWXYZABCDEFGHIJKLMnopqrstuvwxyzabcdefghijklm');

function rot13(message){
    return message
    .split('')
    .map(e => ABC.findIndex(i => e==i ) == -1 ? e : ABC_ENCODE[ABC.findIndex(i => e==i )])
    .join('')
}