const MAX = 140
const EMPTY = 0

function isEmpty(text) {
    return text
        .trim()
        .length == EMPTY
}

function maxLength(text) {
    return text.length >= MAX
}

function generateHashtag (str) {
    if (isEmpty(str)) return false

    const standardization = str
        .trim()
        .replace(/\s+/, ' ')
        .split(" ")
        .map(word => `${word[0].toUpperCase()}${word.slice(1)}`)
        .join("")

    if (maxLength(standardization)) return false
  
    return `#${standardization}`
}