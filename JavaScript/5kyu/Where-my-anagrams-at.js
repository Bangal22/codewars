function anagrams(word, words) {
    word = sortedWords(word);
    return words.filter(e => sortedWords(e) == word);
}
const sortedWords = (value) =>{
    return value.split('')
    .sort()
    .join('')
}