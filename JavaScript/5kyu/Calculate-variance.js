var variance = function(numbers) {
    let mean = numbers.reduce( (acc, el) => acc+el) / numbers.length
    return numbers.reduce( (acc, el) => acc + Math.pow((el-mean), 2), 0) / numbers.length
    
}