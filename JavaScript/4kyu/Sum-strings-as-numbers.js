function sumStrings(a,b) { 
    const regex = /^[0-9]*$/g;

    if(a == '') a = 0
    if(b == '') b = 0

    if (!regex.test(a) && !regex.test(b)) return 0

    return `${BigInt(a)+ BigInt(b)}`;
}