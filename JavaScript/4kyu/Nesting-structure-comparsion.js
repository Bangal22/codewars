Array.prototype.sameStructureAs = function (other) {
    if (Array.isArray(this) != Array.isArray(other)) return false;
    return getArray(this,other,0)
};

function getArray(a, b, i) {
    let index = 0;
    for (i; i < a.length; i++) {
        if( Array.isArray(a[i]) != Array.isArray(b[i])) return false
        if ( typeof(a[i]) == "object" && typeof(b[i]) == "object" ) {
            if( a[i].length != b[i].length ) return false;
        } 
        if(a[i].length == index) return true;
        if( !getArray(a[i], b[i], 0) ) return false;
        index++
    }
    return true;
}