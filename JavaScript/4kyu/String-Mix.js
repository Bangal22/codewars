function getWordsCount(words) {
    const recount = {};
    const w = words.replace(/[^a-z]/g, "")
    for (const i of w) i in recount ? recount[i]++ : recount[i] = 1;
    return recount;
}
function mix(s1, s2) {
    const objectToSort = [];
    const s1Object = getWordsCount(s1);
    const s2Object = getWordsCount(s2);

    for (const letter of "abcdefghijklmnopqrstuvwxyz") {
        const k1 = letter in s1Object && s1Object[letter] != 1 ? s1Object[letter] : 0;
        const k2 = letter in s2Object && s2Object[letter] != 1 ? s2Object[letter] : 0;

        if (k1 > k2) objectToSort.push({ frequency: s1Object[letter], repeat: 1, letter })
        else if (k2 > k1) objectToSort.push({ frequency: s2Object[letter], repeat: 2, letter })
        else { if (k1 || k2) objectToSort.push({ frequency: s2Object[letter], repeat: 3, letter }) }
    }

    objectToSort.sort((a, b) => {
        if (b.frequency - a.frequency == 0) {
            if (a.repeat < b.repeat) return -1
            if (a.repeat > b.repeat) return 1
        }
        return b.frequency - a.frequency
    });
    const result = objectToSort.reduce((acc, el) => acc += `${el.repeat == 3 ? "=" : el.repeat}:${el.letter.repeat(el.frequency)}/`, '');
    return result.slice(0, result.length - 1);
}
