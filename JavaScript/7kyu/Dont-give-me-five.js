function dontGiveMeFive(start, end)
{
  let len = Math.abs(start-(end+1))
  let withOutFive = new Array(len)
  .fill(null)
  .map((e, i) => `${i+start}`)
  .filter(n => !n.includes(5))
  return withOutFive.length;
}