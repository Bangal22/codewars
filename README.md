# Codewars

***

## Name
Codewars katas 

## Description
In this repository you can see the katas that I have made in the Codewars programming web site.


## License
GNU AFFERO GENERAL PUBLIC LICENSE
Version 3, 19 November 2007


