use std::collections::VecDeque;

#[derive(Debug)]
struct Node {
    value: u32,
    left: Option<Box<Node>>,
    right: Option<Box<Node>>,
}

impl Node {
    pub fn new(value: u32) -> Self {
        Node {
            value,
            left: None,
            right: None,
        }
    }

    pub fn left(mut self, node: Node) -> Self {
        self.left = Some(Box::new(node));
        self
    }

    pub fn right(mut self, node: Node) -> Self {
        self.right = Some(Box::new(node));
        self
    }
}

fn tree_by_levels(root: &Node) -> Vec<u32> {
    let mut queue: VecDeque<&Node> = VecDeque::new();
    let mut result: Vec<u32> = Vec::new();
    
    result.push(root.value);
    queue.push_front(root);
    
    while let Some(box_element) = queue.pop_front() {
        if let Some(e) = &box_element.left {
            queue.push_back(e);
            result.push(e.value)
        }
        if let Some(e) = &box_element.right {
            queue.push_back(e);
            result.push(e.value)
        }
    }

    result
}