fn build_string_solution(count: i32, solution:  &mut Vec<String>, prev: i32, current: i32) {
    match count {
       1 => solution.push(prev.to_string()), 
       2 => solution.push(format!("{:?},{:?}", prev, current)),
       _ => solution.push(format!("{:?}-{:?}", prev, current))
    }
 }
 
pub fn range_extraction(a: &[i32]) -> String {
    let mut prev = a[0];
    let mut current = a[1];
    let mut count = 1; 
    let mut solution: Vec<String> = Vec::new();

    for (index, &number) in a.iter().enumerate().skip(1) {
        if (a[index -1]..number).count() == 1 {
            count += 1;
            current = number;
        }
        else {
            build_string_solution(count, &mut solution, prev, current);
            prev = number;
            count = 1;
        }
    }

    build_string_solution(count, &mut solution, prev, current);
    solution.join(",")
}
 