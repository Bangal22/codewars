fn to_digit (numbers: Vec<u64>) -> u64 {
    numbers.iter().fold(0, |acc, &num| acc * 10 + num)
 }
 
fn next_bigger_number(n: u64) -> Option<u64> {
    let mut numbers : Vec<u64> = n
        .to_string()
        .chars()
        .map(|character| character.to_digit(10).unwrap() as u64)
        .collect();

    let mut pivot = 0;
    let mut pivot_index = 0;
    let mut pivot_found = false;

    match numbers.len() {
        1 => return None,
        2 => {
            if numbers[0] > numbers[1] { return None }
            return Some(to_digit(vec![numbers[1], numbers[0]])) 
        }
        _ => {
            for (index, window) in numbers.windows(2).enumerate().rev(){
                if window[0] < window[1] {
                pivot = window[0];
                pivot_index = index;
                pivot_found = true;
                break;
                }
            }

            if !pivot_found { return None }

            let mut substitute = 10;
            let mut substitute_index = 0;

            for (index, number) in numbers.iter().enumerate().skip(pivot_index+1) {
                if number < &substitute && number > &pivot {
                substitute = *number;
                substitute_index = index;
                }
            }

            numbers[pivot_index] = substitute;
            numbers[substitute_index] = pivot;
            numbers[pivot_index+1..].sort_by(|a, b| a.cmp(b));
            
            return Some(to_digit(numbers))
        }
    }
}


 
 
 
 