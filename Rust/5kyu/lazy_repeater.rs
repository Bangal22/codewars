fn make_looper(string: &str) -> impl FnMut() -> char + '_ {
    let mut index = 0;
    let characters: Vec<char> = string.chars().collect();
    move || {
        if characters.len() == index { index = 0}
        let character = characters[index];
        index += 1;
        character
    }
}