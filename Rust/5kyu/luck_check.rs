use std::ops::{AddAssign, Div, Rem, SubAssign};

fn luck_check(ticket: &str) -> Option<bool> {
    if ticket.is_empty() {
        return None;
    }

    let is_odd: bool = ticket.len().rem(2).eq(&0);
    let ticket_len: usize = if is_odd { ticket.len() } else { ticket.len() - 1 };
    let limit: usize = ticket_len.div(2);
    let mut difference: i8 = 0;

    for (index, character) in ticket.chars().enumerate() {
        if !character.is_ascii_digit() { return None }

        if limit == index && !is_odd { continue }

        if limit > index {
            difference.add_assign(character.to_digit(10).unwrap() as i8)
        } else {
            difference.sub_assign(character.to_digit(10).unwrap() as i8)
        }
    }
    Some(difference == 0)
}