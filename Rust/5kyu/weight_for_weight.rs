use std::cmp::Ordering;

fn order_weight(s: &str) -> String {
    let mut map: Vec<(&str, u32)> = s
    .split_whitespace()
    .map(|k| {
        (k,
            k
            .chars()
            .map(|e:char| e.to_digit(10).unwrap())
            .reduce(|acc:u32, el:u32| acc + el)
            .unwrap_or(0))
    }).collect::<Vec<(&str, u32)>>();
    
    map.sort_by(|a: &(&str, u32), b: &(&str, u32)| {
        match (a.1.cmp(&b.1), a.0.cmp(&b.0)) {
            (Ordering::Greater, _) => Ordering::Greater,
            (Ordering::Equal, Ordering::Less) => Ordering::Less,
            _ => Ordering::Greater,   
        }
    });
    map.iter().map(|e:&(&str, u32)| e.0).collect::<Vec<&str>>().join(" ")
}