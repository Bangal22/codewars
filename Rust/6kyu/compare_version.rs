fn main() {
    let result: bool = compare_versions("10.4.6", "10.4");
    println!("{result}");
}

fn compare_versions(version1: &str, version2: &str) -> bool {
    if version1 == version2 { return true }
    let v1: Vec<&str> = version1.split(".").collect();
    let v2: Vec<&str> = version2.split(".").collect();
    return biggest(v1, v2);
}

fn biggest(v1: Vec<&str>, v2: Vec<&str>) -> bool {
    let range: Vec<&str> = if v1.len() > v2.len() { v2.clone() } else { v1.clone() };
    for (i, _v) in range.iter().enumerate() {
        let num1: i32 = v1[i].to_string().parse::<i32>().unwrap();
        let num2: i32 = v2[i].to_string().parse::<i32>().unwrap();
        if num1 > num2 { return true }
        if num1 < num2 { return false }
    }
    return v1.len() > v2.len();
}
