fn multiplication_table(len: usize) -> Vec<Vec<usize>> {
    (1..=len)
        .map(|i: usize| (1..=len).map(|m:usize| m * i).collect::<Vec<usize>>())
        .collect()
}