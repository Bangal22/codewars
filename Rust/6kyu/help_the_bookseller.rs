use std::collections::HashMap;
fn stock_list(list_art: Vec<&str>, list_cat: Vec<&str>) -> String {
    if list_art.len() == 0 || list_cat.len() == 0 { return "".to_string() }
    
    let mut books: HashMap<&str, i16> = HashMap::new();
    let mut result: String = String::new();

    list_art.iter().for_each(|e| {
        let values: Vec<&str> = e.split(" ").collect();
        let key: &str = &values[0][0..1];
        let number: i16 = values[1].parse::<i16>().expect("cannot be converted");
        *books.entry(key).or_default() += number;
    });

    list_cat.iter().for_each(|e| {
        if books.contains_key(e) { result += &format!("({} : {:?}) - ", e, books.get(e).expect("error to get value")) }
        else { result += &format!("({} : {}) - ", e, 0) }
    });

    return result[0..result.len()-3].to_string();
}

