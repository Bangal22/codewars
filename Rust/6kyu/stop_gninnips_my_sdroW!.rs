fn main() {
    let result: String = spin_words("Seriously this is the last one");
    print!("{result}");
}

fn spin_words(words: &str) -> String {
    let v_words: Vec<String> = words.split_whitespace().map(|e:&str|{
        if e.len() >= 5 {
            return e.chars().rev().collect::<String>();
        } 
        return e.to_string();
    }).collect();
    return v_words.join(" ");
}