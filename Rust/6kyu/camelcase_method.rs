fn camel_case(str: &str) -> String {
    str
        .split_whitespace()
        .map(|word| [&word[..1].to_uppercase(), &word[1..]].join(""))
        .collect::<Vec<String>>()
        .join("")
}