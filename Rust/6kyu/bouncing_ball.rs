fn bouncing_ball(h: f64, bounce: f64, window: f64) -> i32 {
    if h <= 0_f64 || bounce <= 0_f64 || bounce >= 1_f64 || window >= h {
        return -1;
    }

    let mut count: i32 = 1;
    let mut rebound_height: f64 = bounce * h;
    while rebound_height > window {
        rebound_height *= bounce;
        count += 2
    }
    count
}
