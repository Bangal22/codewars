fn high(input: &str) -> &str {
    input
        .split_whitespace()
        .rev()
        .max_by_key(|scoring| {
            scoring
                .as_bytes()
                .iter()
                .map(|&byte| (byte - 96) as i32)
                .sum::<i32>()
        })
        .unwrap()
}
