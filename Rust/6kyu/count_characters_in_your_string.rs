use std::collections::HashMap;

fn count(input: &str) -> HashMap<char, i32> {
    input.chars().fold(HashMap::new(), |mut acc, character| {
        *acc.entry(character).or_insert(0) += 1;
        acc
    })
}