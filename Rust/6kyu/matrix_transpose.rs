fn transpose(matrix:  &[Vec<u8>]) -> Vec<Vec<u8>> {
    let mut result: Vec<Vec<u8>> = Vec::new();
    let mut v_transpose: Vec<u8> = Vec::new();
    let mut i: usize = 0;
    let mut j: usize = 0;

    loop {
        v_transpose.push(matrix[i][j]);
        i += 1;
        if i >= matrix.len() {
            i = 0;
            j += 1;
            result.push(v_transpose);
            v_transpose = Vec::new();
        }
        if j >= matrix[0].len() { break }    
    }

    result
}