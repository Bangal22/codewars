fn find_outlier_v1(values: &[i32]) -> i32 {
    let odd = values.iter().filter(|n| *n%2 == 0).collect::<Vec<&i32>>();
    if odd.len() == 1 {
        return **odd.first().unwrap()
    }
    *values.iter().find(|n| *n%2 != 0).unwrap()
}

fn find_outlier_v2(values: &[i32]) -> i32 {
    let (odd, even):(Vec<i32>, Vec<i32>) = values.iter().partition(|v| *v % 2 == 0);
    match odd.len() == 1 {
        true => odd[0],
        false => even[0]
    }
}