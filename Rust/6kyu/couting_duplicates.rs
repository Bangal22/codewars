fn count_duplicates(text: &str) -> u32 {
    let mut counter: u32 = 0;
    let characters: HashSet<char> = text
        .to_ascii_lowercase()
        .chars()
        .into_iter()
        .collect::<HashSet<char>>();

    for v in characters {
        let s = text.to_ascii_lowercase().split(v).count() - 1;
        if s > 1 {
            counter += 1
        }
    }
    return counter;
}
