fn pyramid(n: usize) -> Vec<Vec<u32>> {
    let mut pyr: Vec<Vec<u32>> = Vec::new();
    for x in 1..=n {
       pyr.push(vec![1;x])
    }
    return pyr
}

fn pyramid2(n: usize) -> Vec<Vec<u32>> {
    let mut pyr: Vec<Vec<u32>> = Vec::new();
    for x in 0..n {
        pyr.push(vec![]);
        for _ in 0..x+1 { pyr[x].push(1) } 
    }
    return pyr
}

fn pyramid(n: usize) -> Vec<Vec<u32>> {
    (1..=n).into_iter().map(|e:usize| vec![1;e]).collect::<Vec<Vec<u32>>>()
}