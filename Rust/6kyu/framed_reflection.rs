fn main() {
    let result: String = mirror("srawedoC");
    print!("{result}");
}
fn mirror(text: &str) -> String {
    let text_to_vec: Vec<&str> = text.split(" ").collect();
    let size: usize = text_to_vec.len();
    let mut words: Vec<String> = vec![];
    let mut max: usize = 0;

    for word in text_to_vec {
        let sorted: Vec<&str> = word.split("").collect();
        let reverse: Vec<&str> = sorted.into_iter().rev().collect();
        let joined: String = reverse.join("");
        if max < joined.len() {
            max = joined.len();
        }
        words.push(joined);
    }

    return build_mirror(max, size, words);
}

fn build_mirror(max: usize, size: usize, words: Vec<String>) -> String {
    let mut result: String = String::new();
    for i in 0..size + 2 {
        let mut y: usize = 0;
        if i > 0 && i <= size {
            let str: String = format!("* {}", words[i - 1]);
            result += &str;
            y = words[i - 1].len() + 2;
        }
        loop {
            let mut str: String = String::new();
            if y > max + 3 { break; }
            if i > 0 && i <= size {
                if y < max + 3 { str.push_str(" "); } 
                else { str.push_str("*"); }
            } 
            else { str.push_str("*"); }
            result += &str;
            y = y + 1;
        }
        result += "\n";
    }
    return result.trim_end().to_string();
}
