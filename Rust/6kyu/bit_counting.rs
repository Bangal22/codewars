fn count_bits(n: i64) -> u32 {
    let mut n_mut: i64 = n;
    let mut total_one: u32 = if n % 2 == 0 { 0 } else { 1 };
    while n_mut >= 1 {
        if (n_mut / 2) % 2 != 0 { total_one += 1 }
        n_mut /= 2
    }
    total_one
}