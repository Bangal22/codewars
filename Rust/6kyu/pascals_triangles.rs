fn pascals_triangle(n: usize) -> Vec<usize> {
    let mut pascal_board : Vec<Vec<usize>> = vec![vec![0;n]; n];
    let mut result: Vec<usize> = Vec::new();
   
    for i in 0..n {
        pascal_board[i][0] = 1;
        result.push(pascal_board[i][0]); 
        if i == 0 { continue }
        for j in 0..n {
            if j == n-1 { continue }
            let sum: usize = pascal_board[i-1][j] + pascal_board[i-1][j+1];
            if sum != 0 { 
                pascal_board[i][j+1] = sum ;
                result.push(sum);
            }
        }   
    }
    print!("{:?}", result);
    result
}