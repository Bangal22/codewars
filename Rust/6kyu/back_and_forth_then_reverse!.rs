fn arrange(s: &[i32]) -> Vec<i32> {
    let mut insert_order : bool = true;
    let mut result: Vec<i32> = Vec::new();
    let s_len: usize = (s.len() + (2-1)) / 2;
    (0..s_len).enumerate().for_each(|(i, _)| {
        if insert_order {
            result.push(s[i]);
            result.push(s[s.len() - 1 - i]);
        }
        else {
            result.push(s[s.len() - 1 - i]);
            result.push(s[i]);
        }
        insert_order = !insert_order;
        if (i+1) == s_len && s.len() % 2 != 0 {result.pop();}
    });
    return result;
}
