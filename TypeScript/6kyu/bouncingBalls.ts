export function bouncingBall(h: number, bounce: number, window: number): number {
    if ((h <= 0) || (bounce <= 0 || bounce >= 1) || (window >= h)) return -1
    let count : number = 1;
    let reboundHeight : number = bounce * h 
    while(reboundHeight > window) {
        reboundHeight = bounce * reboundHeight
        count += 2
    }
    return count
}