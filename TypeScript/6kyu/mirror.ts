function mirror(text:string):string{
    let stringMirror : string = text.split('').reverse().join(' ');
    return stringMirror;
}


function main () {
    let result : string = mirror("Hello World");
    console.log(result)
}