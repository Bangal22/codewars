export const pigIt = (a: string): string => {
    let p = a.split(" ");
    let result: string = "";
    p.forEach((element: string, index: number) => {
        let word: any = element.split("");
        let firstWord: string = word[0];
        word.shift();
        if (firstWord != undefined) {
            let ascii = firstWord.toUpperCase().charCodeAt(0);
            if (ascii > 64 && ascii < 91) word.push(firstWord + "ay");
            else word.push(firstWord);
        }
        else word.push(firstWord);

        if (p.length - 1 == index) result += word.join('')
        else result += word.join('') + " ";

    });
    return result;

}