with 

cte_tranches as (
  select 
    deal_id, 
    currency,
    sum(amount) as total_amount
  from tranches 
  group by 1, 2
)

select 
    d.deal_id, 
    deal_name, 
    json_agg(json_build_object('currency', currency, 'total_amount', total_amount) order by currency asc) as currency_details
from deals as d
left join cte_tranches as t using(deal_id)
group by 1
order by 1 desc
