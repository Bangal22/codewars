
def move_zeros(array) :
    return [i for i in array if i] + [i for i in array if not i]

def main() :
    print(move_zeros([9, 0, 0, 9, 1, 2, 0, 1, 0, 1, 0, 3, 0, 1, 9, 0, 0, 0, 0, 9]))

if __name__ == "__main__" :
    main()