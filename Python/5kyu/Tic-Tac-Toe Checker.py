def is_solved(board):
    reuslt = check_winner(board, 1, 2)
    return reuslt


def contains(c, p1, p2):
    if not c.__contains__(0):
        if not c.__contains__(p2): return 1
        if not c.__contains__(p1): return 2
    return False


def check_winner(board, player1, player2):
    contains_zero = False
    diagonalA = []
    diagonalB = []
    for i, j in enumerate(board):

        vertical = []
        horizontal = []

        for k, l in enumerate(board):
            horizontal.append(board[i][k])
            vertical.append(board[k][i])

            if k == i: diagonalA.append(board[i][k])
            if k + i == len(board) - 1: diagonalB.append(board[i][k])

        if not contains_zero:
            contains_zero = horizontal.__contains__(0) or vertical.__contains__(0)

        result = contains(horizontal, player1, player2)
        if result: return result
        result = contains(vertical, player1, player2)
        if result: return result

    result = contains(diagonalA, player1, player2)
    if result: return result
    result = contains(diagonalB, player1, player2)
    if result: return result

    return -1 if contains_zero else 0

def main() :
    result = is_solved([
        [1, 2, 0],
        [0, 1, 2],
        [0, 0, 1],

    ])
    print(result)

if __name__ == "__main__" :
    main()
