import re
def domain_name (domain) :
    return re.sub(r'www[.]+|http:[/]+|https:[/]+|[/]\w+', '', domain).split(".")[0]
def main() :
    data = domain_name("http://google.co.jp")
    print(data)
if __name__ == "__main__" :
    main()