from itertools import combinations


def permutations(string):
    result = string_permutations(list(string), 0, len(string), [])
    print(result)

def string_permutations(s, i, n, r):
    if i!=n :
        for j in range(i, n):
            s[i], s[j] = s[j], s[i]
            if not r.__contains__(''.join(s)): r.append(''.join(s))
            string_permutations(s, i + 1, n, r)

    return r

def main():
    (permutations("ab"))


if __name__ == "__main__":
    main()
