# Version 1
def permutations(s):
    def backtrack(array, current_array, array_state, result):
        if len(current_array) == len(array) :
            result.append(''.join(current_array) )
            return
        
        for index in range(len(array)):
            if not array_state[index] :
                array_state[index] = True
                current_array.append(array[index])
                backtrack(array, current_array, array_state, result)
                array_state[index] = False
                current_array.pop()
        
        return result
    
    return set(backtrack(list(s), [], [False] * len(s), []))


# Version 2
def permutations(s):
    if len(s) <= 1:
        return [s]

    def backtracking(array, start, result):
        if start == len(array) - 1 :
            result.append(''.join(array))
            return
    
        for index in range(start, len(array)) :
            array[start], array[index] = array[index], array[start]
            backtracking(array, start+1, result)
            array[start], array[index] = array[index], array[start]

        return result 
    
    return set(backtracking(list(s), 0, []))