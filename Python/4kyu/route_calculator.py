import re

SUM = "+"
SUBSTRACT = "-"
MULTIPLY = "*"
DIVIDE = "$"

def calculate(expression) :
    if re.search(r'[^\d$*+-.]', expression) : return "400: Bad request"

    for i in [DIVIDE, MULTIPLY, SUBSTRACT, SUM] :
        perform_operation = False
        op = None
        route = separator(expression)
        idx = 0

        while idx < len(route) :
            if route[idx] == i:
                op = route[idx]
                perform_operation = True
                idx = idx + 1
            else:

                if perform_operation:
                    result = 0

                    a = float(route[idx - 2])
                    b = float(route[idx])

                    if op == SUM: result = a + b
                    elif op == SUBSTRACT: result = a - b
                    elif op == MULTIPLY: result = a * b
                    elif op == DIVIDE: result = a / b

                    to_replace = '{}{}{}'.format(route[idx - 2], op, route[idx])
                    expression = expression.replace(to_replace, str(result))
                    route = separator(expression)
                    perform_operation = False
                    idx = -1

                idx = idx + 1


    return  float(route[0]) if int(route[0].split('.')[1]) >= 1 else int(float(route[0]))






def separator(expression) :
    numbers = re.sub(r'[$*+-]', ' - ', expression).split()
    operator = list(filter(lambda x: (x and x != "."), re.split(r'[\d]', expression)))
    for idx, i in enumerate(numbers):
        if i == "-": numbers[idx] = operator.pop(0)
    return numbers




def main() :
    result = calculate("1*p3")
    print(result)

if __name__ == "__main__" :
    main()