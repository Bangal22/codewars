

def limit(pos, length) :
    return length > pos

def snail (array) :
    if not len(array[0]) : return [[]]
    result = []
    i = 0
    j = 0
    go_down = False
    go_right = False
    go_up = False

    index = 0
    count = 0
    array_length = len(array)-1
    total_length = len(array) * len(array)

    while(count != total_length) :
        result.append(array[i][j])

        if j == array_length and not go_down :
            go_down = True
        elif i == array_length and go_down:
            go_down = False
        if i == j and not go_right and i == array_length and j == array_length:
            go_right = True
        if i == array_length and j == index :
            go_right = False
            go_up = True
            index = index+1
        if i == index and go_up :
            array_length = array_length-1
            go_up = False


        if limit(j, array_length) and not go_down and not go_right and not go_up:
            j = j+1
        elif limit(i, array_length) and go_down :
            i = i+1
        elif limit(index, j) and (go_right) :
            j = j-1
        elif go_up :
            i = i-1


        count = count+1
    return result



def main():
    array = [[]]
    result = snail(array)  # => [1,2,3,6,9,8,7,4,5]
    print(result)

if __name__ == "__main__" :
    main()