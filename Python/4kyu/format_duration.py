import math
def format_duration(seconds):
    if seconds == 0 : return "now"

    y = math.floor(seconds / 31536000)
    d = math.floor((seconds % 31536000) / 86400)
    h = math.floor((seconds % 86400) / 3600)
    m = math.floor(((seconds % 86400) % 3600) / 60)
    s = ((seconds % 86400) % 3600) % 60

    year = "" if not y else "{} year" if y == 1 else "{} years"
    day = "" if not d else "{} day" if d == 1 else "{} days"
    hour = "" if not h else "{} hour" if h == 1 else "{} hours"
    minute = "" if not m else "{} minute" if m == 1 else "{} minutes"
    second = "" if not s else "{} second" if s == 1 else "{} seconds"

    time = list(filter(lambda t: t != "", [year.format(y), day.format(d), hour.format(h), minute.format(m), second.format(s)]))

    format_result = {
        0: "now",
        1: "{}",
        2: "{} and {}",
        3: "{}, {} and {}",
        4: "{}, {}, {} and {}",
        5: "{}, {}, {}, {} and {}"
    }

    return format_result[min(len(time), 5)].format(*time).lstrip()

def main() :
    print(format_duration(31537126))
    #31537126

if __name__ == "__main__" :
    main()
