def next_matches (old_positions: list[int], new_positions: list[int]) -> list[int] :
    for index, _ in enumerate(old_positions) :
        if index == 0: continue 
        if index == 1: new_positions[index] = old_positions[len(old_positions)-1]
        else: new_positions[index] = old_positions[index-1] 
    return new_positions


def create_rounds (first_teams: list[int], second_teams: list[int]) -> list[(int, int)]:
    return [(team1, team2) for team1, team2 in zip(first_teams, second_teams)]


def build_matches_table(teams: int) -> list[list[(int, int)]]:
    calendar = []
    clubs = list(range(1, teams+1))
    days = range(teams - 1)
    middle = int(teams / 2)

    for _ in days :
        first_teams = clubs[0:middle]
        second_teams = clubs[middle: len(clubs)]
        calendar.append(create_rounds(first_teams, reversed(second_teams)))
        clubs = next_matches(clubs, first_teams + second_teams)

    return calendar

def main() :
    result = build_matches_table(8)
    print(result)

if __name__ == '__main__' :
	main()