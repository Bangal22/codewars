import re

def top_3_words(text):
    words = {}
    text = re.sub(r"[\.,-\/#!$%\^&\*;:{}=\-_`~()?¿]", ' ', text.lower())
    for x in text.split(" "):
        if x in words: words[x] += 1
        else: words.setdefault(x, 1)

    words = sorted(words.items(), key=lambda x: x[1], reverse=True)
    words = list(map(lambda x: x[0], words))
    words = list(filter(filter_words, words))
    x = slice(0, 3)
    return words[x]


def filter_words(c):
    if re.match(r"[']", c) and not re.search(r"[\w]", c):
        return ''
    return c
