
def strip_comment(strng, markers) :
    s = strng.split("\n")
    result = ""
    for l, i in enumerate(s) :
        f = False
        for idx, x in enumerate(i) :
            if markers.__contains__(x) : result += i[0: idx].rstrip(); f = True ; break
        if not f : result += i.rstrip()

        if not len(s) -1 == l: result += "\n"
    
    return result
def main () :
    strip_comment('a #b\nc\n', ['#', '$'])

if __name__ == "__main__" :
    main()