
def solution (string, ending):
    return string[(len(string)-1) - (len(ending)-1) : len(string)] == ending
def main() :
    print(solution("abcde", "s"))

if __name__ == "__main__" :
    main()