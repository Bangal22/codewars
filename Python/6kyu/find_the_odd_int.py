def find_it (seq) :
    max = 0
    save_seq = {}
    for i in seq :
        if save_seq.__contains__(i) : save_seq[i] += 1
        else : save_seq.setdefault(i, 1)

    for i in save_seq : max = i if save_seq[i] > max and not save_seq[i] % 2 == 0 else max

    return max

def main () :
    print(find_it([20,1,-1,2,-2,3,3,5,5,1,2,4,20,4,-1,-2,5]))

if __name__ == "__main__" :
    main()

# Solucion con flow

# def find_it(seq):
#     for i in seq:
#         if seq.count(i)%2!=0:
#             return i
