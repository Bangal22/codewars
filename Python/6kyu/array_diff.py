def array_diff(a, b):
    if not len(a): return []
    for i in b:
        allDeleted = not i in a
        while not allDeleted:
            a.remove(i)
            allDeleted = not i in a
    return a


def main():
    array_diff([11, 2, -1, -19, -6, 4, -19, 11, -2, 1], [12, 16, 2, 0, -8, -16, 5, 0, 17, -17, 15, -3])


if __name__ == "__main__":
    main()
