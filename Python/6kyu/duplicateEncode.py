
def duplicate_encode(word) :
    result = ""
    characters = {}
    word = word.lower()
    for i in word :
        if characters.__contains__(i) :characters[i] = ")"
        else : characters.setdefault(i,"(")

    for i in word : result += characters[i]

    return result

def main() :
    print(duplicate_encode("Success"))

if __name__ == "__main__" :
    main()