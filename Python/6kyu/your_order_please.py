def order(sentence):
    if sentence == "" : return ""
    sort_array = [None] * len(sentence.split(" "))

    for i in sentence.split(" "):
        for j in i:
            if j.isdigit(): sort_array[int(j) - 1] = i + " "

    return ''.join(sort_array).rstrip()

def main():
    print(order(""))


if __name__ == "__main__":
    main()
