def solution(s) :
    s_with_spaces = ""
    for i in s :
        if i.isupper() : s_with_spaces +=  " "+i
        else : s_with_spaces += i
    return s_with_spaces.lstrip()
def main () :
    data = solution("BreakCamelCase")
    print(data)
if __name__ == "__main__" :
    main()