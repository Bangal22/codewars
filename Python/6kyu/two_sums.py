
def two_sum(numbers,target):
    result = []
    find = False
    for idxI, i in enumerate(numbers) :
        for idxY,y in enumerate(numbers) :
          if idxI != idxY :
              if y+i == target: result = [idxY, idxI] ; find = True ;break
        if find : break
    return  result

def main() :
    print(two_sum([1, 2, 3], 4))

if __name__ == "__main__" :
    main()