def likes(names) :
    if not len(names): return "no one likes this"
    if len(names) <= 3:
        if len(names) == 1: return names[0] + " like this"
        else: return ' and '.join(names) + " likes this" if len(names) == 2 else ', '.join(names[0:2]) + ' and ' + names[2] + " likes this"
    else: return ', '.join(names[0:2]) + ' and ' + str(len(names[2: len(names)])) + " others likes this"


def main() :
    data = likes(["Max"])
    print(data)
if __name__ == "__main__" :
    main()