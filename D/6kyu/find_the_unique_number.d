import std.stdio;
import std.conv;
import std.math;
import std.random;
import std.datetime;
import std.array;
import std.ascii;
import std.string;
import std.format;
import std.typecons : No;
import std.typecons;
import std.range : chain;
import std.file;
import core.thread;
import std.concurrency;
import std.parallelism;
import std.range;
import std.algorithm;
import std.container;

void main()
{
    int result = findUniq([1, 1, 1, 2, 1, 1]);
    writeln(result);
}

int findUniq(const int[] arr)
{
    int last = arr[0];
    ulong len = arr.length;

    for (int i = 0; i < arr.length; i++)
    {
        if (arr[i] != last) {
            if (i+1 > len-1) return arr[i];

            if (arr[i+1] == last) return arr[i];
            else return last;
        }
    }
    return last;
}
